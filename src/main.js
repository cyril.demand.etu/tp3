import Router from './Router';
import data from './data';
import PizzaList from './pages/PizzaList';
import Component from './components/Component.js'
import PizzaForm from './pages/PizzaForm.js';

Router.titleElement = document.querySelector('.pageTitle');
Router.contentElement = document.querySelector('.pageContent');

const pizzaList = new PizzaList(data),
	aboutPage = new Component('p', null, 'ce site est génial'),
	pizzaForm = new PizzaForm();


Router.routes = [{ path: '/', page: pizzaList, title: 'La carte' }];

pizzaList.pizzas = data; // appel du setter

document.querySelector('.logo').innerHTML += `<small>les pizzas c'est la vie</small>`;

document.querySelector('.pizzaListLink').setAttribute('class','pizzaListLink active');

document.querySelector('.newsContainer').setAttribute('style','');

document.querySelector('.newsContainer').addEventListener('click', event => {
	// ici pas de problème de scope, `this` est préservé
	event.preventDefault();
	document.querySelector('.newsContainer').setAttribute('style','display:none'); // ok
});



    Router.routes = [
	{ path: '/', page: pizzaList, title: 'La carte' },
	{ path: '/a-propos', page: aboutPage, title: 'À propos' },
	{ path: '/ajouter-pizza', page: pizzaForm, title: 'Ajouter une pizza' },
];

Router.menuElement = document.querySelector('.mainMenu');

document.querySelector('.newsContainer').addEventListener('click', event => {
	// ici pas de problème de scope, `this` est préservé
	event.preventDefault();
	document.querySelector('.newsContainer').setAttribute('style','display:none'); // ok
});

Router.navigate('/'+document.location.toString().split('/')[document.location.toString().split('/').length - 1]);

window.onpopstate = function(event) {
    Router.navigate('/'+document.location.toString().split('/')[document.location.toString().split('/').length - 1]);
};