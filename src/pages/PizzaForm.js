import Page from './Page.js';

export default class PizzaForm extends Page {
	render() {
		return /*html*/ `
			<form class="pizzaForm">
				<label>
					Nom :
					<input type="text" name="name">
				</label>
				<button type="submit">Ajouter</button>
			</form>`;
	}

	mount(element) {
		super.mount(element);
        this.element.querySelector('button[type=submit]').addEventListener('click', event =>{
            event.preventDefault();
            this.submit(event);
        });
	}

	submit(event) {
        const name = this.element.querySelector('input[name=name]');

        if(name.value==''){
            alert('le champ est vide');
        }else{
            alert(`la pizza ${name.value} a été ajoutée`);
        }
    }
}
